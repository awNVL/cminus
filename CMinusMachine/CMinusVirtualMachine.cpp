/* C Minus virtual machine - simplified TINY machine implementation used to execute CMinus instruction */
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
using std::cin;
using std::cout;
using std::endl;
using std::ifstream;

typedef enum{
	opclRO,		// register only instruction
	opclRM,		// register-memory instruction
	opclRA,		// register-absolute address instruction
} opCLASS;

typedef enum{
	/* opcode r,s,t */
	/* RO instruction */
	opHALT,		// stop execution
	opIN,			// read value to reg[r]
	opOUT,		// ouput the value of reg[r]
	opADD,		// reg[r] = reg[s] + reg[t]
	opSUB,		// reg[r] = reg[s] - reg[t]
	opMUL,		// reg[r] = reg[s] * reg[t]
	opDIV,		// reg[r] = reg[s] / reg[t]
	opROLim,	// limitation of RO code
	
	/* opcode r,d(s)  a = d + reg[s] */
	/* RM instruction */
	opLD,			// reg[r] = dMem[a]
	opST,			// dMem[a] = reg[s]
	opRMLim,	// limitation of RM code
	
	/* RA instruction */
	opLDA,		// reg[r] = a
	opLDC,		// reg[r] = d
	opJLT,		// if(reg[r]<0) reg[PC_REG] = a
	opJLE,		// if(reg[r]<=0) reg[PC_REG] = a
	opJGT,		// if(reg[r]>0) reg[PC_REG] = a
	opJGE,		// if(reg[r]>=0) reg[PC_REG] = a
	opJEQ,		// if(reg[r]==0) reg[PC_REG] = a
	opJNE,		// if(reg[r]!=0) reg[PC_REG] = a
	opRALim		// limitation of RA code
} opCODE;

/* For code lookup */
char *opCodeTab[]={
										/* RO instruction */
										"HALT","IN","OUT","ADD","SUB","MUL","DIV","????",
										/* RM instruction */
										"LD","ST","????",
										/* RA instruction */
										"LDA","LDC","JLT","JLE","JGT","JGE","JEQ","JNE","????"
									};

/* Result after execution of each instruction */ 												
typedef enum{
	stepOK,
	stepHALT,
	stepIMEM_ERR,
	stepDMEM_ERR,
	stepZERO_DIV
} stepResult;

/* An instruction record */			
typedef struct{
	opCODE insOp;
	int insArg1;
	int insArg2;
	int insArg3;
} Instruction;

/* memory area and register information */
ifstream source;			// source file
const int NO_REGS=8;	// number of registers
const int PC_REG=7;		// program counter register
const int IADDR_SIZE=1024;	// size of instruction memory
const int DADDR_SIZE=1024;	// size of data memory

int reg[NO_REGS];		// register
Instruction iMem[IADDR_SIZE];		// instruction memory
int dMem[DADDR_SIZE];		// data memory

/* read file */
const int SIZE=256;		// max length of a line
const int WORDLEN=20;	// max length of a word
int lineLen=0;		// length of a line
int col=0;				// current position of a line
char lineBuf[SIZE];		// line buffer
char ch;	// current character of a line
int lineNo;		// line number
char word[WORDLEN];		// get a word
int num;		// get a number

void getChar(){
	if(++col<lineLen)
		ch=lineBuf[col];
	else
		ch=' ';
}

bool skipBlank(){
	while((col<lineLen)&&((ch==' ')||(ch=='\t')||(ch=='\n')))
		getChar();
	if(col<lineLen)
		return true;
	else
		return false;
}

bool getNum(){
	if(skipBlank()){
		num=0;
		int len=0;
		int term=1;
		if(ch=='+')
			getChar();
		if(ch=='-'){
			term=-1;
			getChar();
		}
		if(!isdigit(ch))
			return false;
		while(isdigit(ch)){
			num=num*10+(ch-'0')*term;
			getChar();
		}
		return true;
	}
	else
		return false;
}

bool getWord(){
	if(skipBlank()){
		if(!isalpha(ch))
			return false;
		int len=0;
		while(isalpha(ch)){
			word[len++]=ch;
			getChar();
		}
		word[len]='\0';
		return true;
	}	
	else
		return false;
}

bool skipChar(char c){
	if((skipBlank())&&(ch==c)){
		getChar();
		return true;
	}
	else
		return false;
}

void error(char *c,int line){
	cout<<c<<" in line:"<<line<<endl;
	exit(1);
}

void writeInstruction(){
	/* opcode and registers */
	opCLASS opclass;
	opCODE op;
	int arg1;
	int arg2;
	int arg3;
	
	for(int i=0;i<NO_REGS;i++)
		reg[i]=0;
		
	dMem[0]=DADDR_SIZE-1;
	for(int i=1;i<DADDR_SIZE;i++)
		dMem[i]=0;
	
	for(int i=0;i<IADDR_SIZE;i++){
		iMem[i].insOp=opHALT;
		iMem[i].insArg1=0;
		iMem[i].insArg2=0;
		iMem[i].insArg3=0;
	}
	
	int loc=0;	// for instruction number
	while(!source.eof()){
		source.getline(lineBuf,SIZE);
		lineLen=strlen(lineBuf);
		col=0;
		ch=lineBuf[col];
		/* When met a blank line,skip it */
		if(!skipBlank())
			continue;
		/* When met a comment line,skip it */
		if(ch=='*')
			continue;
		if(!getNum())
			error("Bad location",loc);
		loc=num;
		if(!skipChar(':'))
			error("Missing colon",loc);
		if(!getWord())
			error("Missing opcode",loc);
		for(int i=0;i<opRALim+1;i++){
			if(strcmp(word,opCodeTab[i])==0){
				op=static_cast<opCODE>(i);
				if(op<opROLim)
					opclass=opclRO;
				else if(op<opRMLim)
					opclass=opclRM;
				else
					opclass=opclRA;
				break;
			}
			if(i==opRALim){
				error("Wrong opcode",loc);
			}
		}
	
		switch(opclass){
			case opclRO:
				if(!getNum())
					error("Bad first register",loc);
				arg1=num;
				if(!skipChar(','))
					error("Missing comma",loc);
				if(!getNum())
					error("Bad second register",loc);
				arg2=num;
				if(!skipChar(','))
					error("Missing comma",loc);
				if(!getNum())
					error("Bad third register",loc);
				arg3=num;
				break;
			case opclRM:
			case opclRA:
				if(!getNum())
					error("Bad first register",loc);
				arg1=num;
				if(!skipChar(','))
					error("Missing comma",loc);
				if(!getNum())
					error("Bad displacement",loc);
				arg2=num;
				if(!skipChar('('))
					error("Missing left paren",loc);
				if(!getNum())
					error("Bad second register",loc);
				arg3=num;
				if(!skipChar(')'))
					error("Missing right paren",loc);
				break;
			default:
				break;
		}
		iMem[loc].insOp=op;
		iMem[loc].insArg1=arg1;
		iMem[loc].insArg2=arg2;
		iMem[loc].insArg3=arg3;
	}
}

stepResult readInstruction(){
	int pc;
	int r,s,t,m;
	opCLASS opclass;
	pc=reg[PC_REG];
	reg[PC_REG]=pc+1;
	Instruction curInstruction=iMem[pc];
	
	if(curInstruction.insOp<opROLim){
		opclass=opclRO;
		r=curInstruction.insArg1;
		s=curInstruction.insArg2;
		t=curInstruction.insArg3;
	}
	else if(curInstruction.insOp<opRMLim){
		opclass=opclRM;
		r=curInstruction.insArg1;
		t=curInstruction.insArg3;
		m=curInstruction.insArg2+reg[t];
		/* Only reference to dMem[m] may generates data memory error,once m is illegal */
		if((m<0)||(m>DADDR_SIZE-1))
			return stepDMEM_ERR;
	}
	else{
		/* RA instruction has no reference to dMem[m],so no data memory error will generate here */
		opclass=opclRA;
		r=curInstruction.insArg1;
		t=curInstruction.insArg3;
		m=curInstruction.insArg2+reg[t];
	}
	
	switch(opclass){
		case opclRO:
			switch(curInstruction.insOp){
				case opHALT:
					return stepHALT;
					break;
				case opIN:
					cout<<"In instruction,please input: ";
					cin>>reg[r];
					break;
				case opOUT:
					cout<<reg[r]<<endl;
					break;
				case opADD:
					reg[r]=reg[s]+reg[t];
					break;
				case opSUB:
					reg[r]=reg[s]-reg[t];
					break;
				case opMUL:
					reg[r]=reg[s]*reg[t];
					break;
				case opDIV:
					if(reg[t]==0)
						return stepZERO_DIV;
					reg[r]=reg[s]/reg[t];
					break;
				default:
					break;
			}
			break;
			
		case opclRM:
			switch(curInstruction.insOp){
				case opLD:
					reg[r]=dMem[m];
					break;
				case opST:
					dMem[m]=reg[r];
					break;
				default:
					break;
			}
			break;
			
		case opclRA:
			switch(curInstruction.insOp){
				case opLDA:
					reg[r]=m;
					break;
				case opLDC:
					reg[r]=curInstruction.insArg2;
					break;
				case opJLT:
					if(reg[r]<0)
						reg[PC_REG]=m;
					break;
				case opJLE:
					if(reg[r]<=0)
						reg[PC_REG]=m;
					break;
				case opJGT:
					if(reg[r]>0)
						reg[PC_REG]=m;
					break;
				case opJGE:
					if(reg[r]>=0)
						reg[PC_REG]=m;
					break;
				case opJEQ:
					if(reg[r]==0)
						reg[PC_REG]=m;
					break;
				case opJNE:
					if(reg[r]!=0)
						reg[PC_REG]=m;
					break;
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
	return stepOK;
}

int main(){
	char filename[100]="\0";
	cout<<"please input the file name with it's absoulte address to execute:"<<endl;
	cin.getline(filename,100);
	source.open(filename);
	if(!source){
		cout<<"source open failed."<<endl;
		exit(1);
	}
	writeInstruction();
	source.close();
	
	stepResult done=stepOK;
	/* Once step result is not ok,stop reading instruction,and check the type of result */
	while(done==stepOK)
		done=readInstruction();
	
	switch(done){
		case stepHALT:
			cout<<"program terminated."<<endl;
			break;
		case stepDMEM_ERR:
			cout<<"data memory error"<<endl;
			break;
		case stepIMEM_ERR:
			cout<<"instruction memory error"<<endl;
			break;
		case stepZERO_DIV:
			cout<<"zero division error"<<endl;
			break;
		default:
			break;
	}
	
	char end;
	cout<<"exit?"<<endl;
	cin>>end;	
	return 0;
}
