/* C Minus - globals */
#ifndef GLOBALS_H
#define GLOBALS_H
#include <iostream>
#include <iomanip>
#include <fstream>
using std::cin;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::setw;

extern ifstream source;
extern ofstream info;
extern ofstream temp;
extern ofstream code;
extern ifstream readtoken;

extern int lineNo;
extern const int WIDTH;
const int CHILD=3;	// max number of node child

typedef enum{
	ELSE,IF,INT,RETURN,VOID,WHILE,		// key words
	/* special symbols */
	PLUS,MINUS,TIMES,OVER,						// arithmetic operators
	LT,LE,GT,GE,EQ,NE,								// comparision operators
	ASSIGN,SIMI,COMMA,								// other operators
	LP,RP,LSB,RSB,LCB,RCB,						// brackets operators
	ID,NUM,														// other tokens
	ENDFILE,													// end of file token
	UNKNOWN,													// error token
} TokenType;

/* states for token DFA */
typedef enum{
	Sstart,Sid,Snum,Sle,Sge,Seq,Sne,	// we deleted state for operator which has only one character
	Ssimi,Scomma,											// and for those share a common prefix we keep the state which has longer operator
	Slp,Srp,Slsb,Srsb,Slcb,Srcb,Scomment,Serror,Sdone
} StateType;

/* syntax tree node */
typedef enum{ FunK,StmtK,ExpK } NodeKind;
typedef enum{ DeclF,CallF } FunKind;
typedef enum{ ExpS,CompS,IfS,WhileS,ReturnS } StmtKind;
typedef enum{ AssignE,OpE,ConstE,IdE,ArrE } ExpKind;
typedef enum{ Void,Integer,Array,Function } TypeKind;

typedef struct Tree{
	NodeKind nodekind;
	TokenType typekind;
	TypeKind type;		// used for type checker
	int lineno;
	union{
		FunKind fun;
		StmtKind stmt;
		ExpKind exp;
	} subkind;
	struct{
		TokenType op;		// record operator for opeartor node
		int val;				// record value for const node,or for array size
		char *name;			// record name for identifier
	} attr;
	Tree *child[3];
	Tree *sibling;
} TreeNode;

extern TreeNode *funcTree;
#endif
