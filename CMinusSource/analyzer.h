/* C Minus semantic analyzer - header */
#ifndef ANALYZER_H
#define ANALYZER_H
#include "globals.h"
#include "utilities.h"
#include "parser.h"
#include "symbol.h"

/* Procedure to build symbol table */
void buildSymTab(TreeNode *t);

/* Procedure to check and set type information */
void typeCheck(TreeNode *t);

/* Procedure to print symbol table in indention formation */
void printSymbolTable(TreeNode *t);

/* Procedure use build symbol table as internal recursive */
void buildSymbolTable(TreeNode *syntaxTree);

/* Procedure use type check as internal recursive */
void typeChecking(TreeNode *syntaxTree);
#endif
