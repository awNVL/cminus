/* C Minus symbol table - header */
#ifndef SYMBOL_H
#define SYMBOL_H
#include "globals.h"
#include "utilities.h"
#include "parser.h"

const int SIZE=103;	// size of a hash table,should be a prime
const int SHIFT=4;		// multiple factor in hash function

typedef struct Record{
	char *name;				// for identifier name,NULL for compound record
	int offset;				// offset in current activation record for variable,entry for function
	TypeKind type;		// type of identifier
	struct Record* next;	// next record of current record
	struct Table* nextTab;	// if it's a function-declaration record or compound record,create a symbol table for its scope
} hashRecord;

typedef struct Table{
	struct Table* preLayer;				// to it's preceding symbol
	struct Record* table[SIZE];		// the hash table
} symTable;

extern symTable *globolSymtab;				// globol symbol table

/* Function to insert a record to symbol table */
symTable* symtabInsert(char *id,TypeKind type,symTable *symtab);

/* Function to look up a record in symbol table */
hashRecord* symtabLookup(char *id,symTable *symtab);

/* Function to look up a record in symbol table for compound statement */
hashRecord* symtabLookup(int index,symTable *symtab);

/* Function to look up a record in local symbol table */
hashRecord *localLookup(char *id,symTable *symtab);

/* Function to look up a record in globol symbol table */
hashRecord* globolLookup(char *id);

/* Function to add predefined function in globol symbol table */
void addPredefined();
#endif
