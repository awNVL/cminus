/* C Minus code generator - definition */
#include "codegen.h"

/* offset for runtime environment */
const static int ofpFO=0;
const static int retFO=-1;
const static int initFO=-2;
const static int NOPARA=0;	// to initialize last paramter's offset and check if a function have parameter or not
static int frameoffset=0;		// size of current activation record
static int globolsize=0;		// size of globol area
static int lastparaoffset=0;		// offset of the last parameter of current activation record
static int savedLocMain=0;		// save location for main procedure to later backpatch
static bool isMain=false;		// to check if we are in main procedure or not
static bool isArrayIdInCall=false;		// to check if a single array identifier is appeared in a function call
static bool isCallInAssign=false;		// to check if a function call is in assign statement
static bool isCallInOp=false;		// to check if a function call is in an operation
static symTable *currentScope=NULL;
static symTable *globolScope;

/* procedure in help generate code */
static void standardPrelude();
static void saveForMain();
static void jumpToMain();
static void allocateGlobolArea(TreeNode *t);
static void initalizeFramePointer();
static void usedBeforeDeclaration(char *id,int line);
static TreeNode *findDeclNode(TreeNode *function,char *id);
static void checkArgForInput(TreeNode *arg);
static void checkArgForOutput(TreeNode *arg);
static void checkArgPara(TreeNode *fun,TreeNode *arg,TreeNode *para);
static bool isParameter(int offset);
static void checkArrayIndex();
static void checkCallInAssign(TreeNode *decl,TreeNode *call);
static void checkCallInOp(TreeNode *decl,TreeNode *call);
static void allocateSpaceForParameter(TreeNode *t);
static void allocateSpaceForVariable(TreeNode *t);
static void genFun(TreeNode *t);
static void genStmt(TreeNode *t);
static void genExp(TreeNode *t);
static void cGen(TreeNode *t);

/* procedure to initialize the runtime environment */
static void standardPrelude(){
	emitRM("LD",gp,0,ac,"load gp with max address");
	emitRM("LDA",fp,0,gp,"copy gp to fp");
	emitRM("ST",ac,0,ac,"clear location 0");
}

/* procedure to save a location for main procedure */
static void saveForMain(){
	savedLocMain=emitLocation();
	int loc=emitSkip(1);
}

/* procedure to jump to main procedure to execute */
static void jumpToMain(){
	int loc=emitLocation();
	emitBackpatch(savedLocMain);
	emitRM_Abs("LDA",pc,loc,"jump to main");
	emitRestore();
}

/* procedure to allocate space for globol variable and array */
static void allocateGlobolArea(TreeNode *t){
	if(t!=NULL){
		if(t->nodekind==ExpK){
			hashRecord *rec=NULL;
			switch(t->subkind.exp){
				case IdE:{
					rec=globolLookup(t->attr.name);
					if(rec==NULL)
						usedBeforeDeclaration(t->attr.name,t->lineno);
					rec->offset=globolsize--;
					break;
				}
				
				case ArrE:{
					rec=globolLookup(t->attr.name);
					if(rec==NULL)
						usedBeforeDeclaration(t->attr.name,t->lineno);
					rec->offset=globolsize--;
					int arraysize=t->attr.val;
					globolsize=globolsize+1-arraysize;
					break;
				}
				
				default:
					break;
			}
		}
	}
}

/* procedure to initialize frame pointer before the execution of main procedure  */
static void initalizeFramePointer(){
	emitRM("LDA",fp,globolsize,gp,"initialize frame pointer");
}

/* procedure to generate a error message for undeclared indentifier */ 
static void usedBeforeDeclaration(char *id,int line){
	cout<<"Used before declaration: "<<id<<" ,in line: "<<line<<endl;
	exit(1);
}

/* Function to find the declaration node of a function */
static TreeNode *findDeclNode(TreeNode *function,char *id){
	while(function!=NULL){
		if((function->nodekind==FunK)&&(function->subkind.fun==DeclF)){
			if(strcmp(function->attr.name,id)==0)
				return function;
		}
		function=function->sibling;
	}
}

/* procedure to check the argument of function call input */
static void checkArgForInput(TreeNode *arg){
	if(arg!=NULL){
		cout<<"argument too many in function : input"<<endl;
		exit(1); 
	}
}

/* procedure to check the argument of function call output */
static void checkArgForOutput(TreeNode *arg){
	if(arg==NULL){
		cout<<"argument too few in function : output"<<endl;
		exit(1);
	}
	if((arg!=NULL)&&(arg->sibling!=NULL)){
		cout<<"argument too many in function : output"<<endl;
		exit(1);
	}
	if((arg->nodekind==ExpK)&&(arg->subkind.exp==IdE)){
		symTable *scope=currentScope;
		hashRecord *argrec=symtabLookup(arg->attr.name,scope);
		if(argrec->type==Array){
			cout<<"argument type match error in function : output"<<endl;
			exit(1);
		}
	}
	if((arg->nodekind==FunK)&&(arg->subkind.fun==CallF)){
		TreeNode *fun=findDeclNode(funcTree,arg->attr.name);
		if(fun==NULL){		// case for predefined function,input and output
			cout<<"argument type match error in function : output"<<endl;
			exit(1);
		}
		if(fun->typekind==VOID){
			cout<<"argument type match error in function : output"<<endl;
			exit(1);
		}
	}	
}

/* procedure to check the argument of function call */ 
static void checkArgPara(TreeNode *fun,TreeNode *arg,TreeNode *para){
	if((para==NULL)&&(arg!=NULL)){
		cout<<"argument too many in function : "<<fun->attr.name<<endl;
		exit(1);
	}
	if((para!=NULL)&&(arg==NULL)){
		cout<<"argument too few in function : "<<fun->attr.name<<endl;
		exit(1);
	}
	if((para!=NULL)&&(arg!=NULL)){
		symTable *scope=currentScope;
		hashRecord *argrec=NULL;
		if(para->nodekind!=ExpK){
			cout<<"wrong argument in checkArgPara"<<endl;
			exit(1);
		}
		if(para->subkind.exp==IdE){
			if((arg->nodekind==ExpK)&&(arg->subkind.exp==IdE)){
				argrec=symtabLookup(arg->attr.name,scope);
				if(argrec->type==Array){
					cout<<"argument type match error in function : "<<fun->attr.name<<endl;
					exit(1);
				}
			}
			if((arg->nodekind==FunK)&&(arg->subkind.fun==CallF)){
				TreeNode *argfun=findDeclNode(funcTree,arg->attr.name);
				if(argfun==NULL){		// case for predefined function,input and ouput
					cout<<"argument type match error in function : "<<fun->attr.name<<endl;
					exit(1);
				}
				if(argfun->typekind==VOID){
					cout<<"argument type match error in function : "<<fun->attr.name<<endl;
					exit(1);
				}
			}
		}
		if((para->subkind.exp==ArrE)&&(arg->nodekind==FunK)&&(arg->subkind.fun==CallF)){
			cout<<"argument type match error in function : "<<fun->attr.name<<endl;
			exit(1);
		}
		if((para->subkind.exp==ArrE)&&(arg->nodekind==ExpK)){
			if(arg->subkind.exp!=IdE){
				cout<<"argument type match error in function : "<<fun->attr.name<<endl;
				exit(1);
			}
			if(arg->subkind.exp==IdE){
				argrec=symtabLookup(arg->attr.name,scope);
				if(argrec->type!=Array){
					cout<<"argument type match error in function : "<<fun->attr.name<<endl;
					exit(1);
				}	
			}
		}
	}
}

/* Function to check if an identifier is a parameter or a local array,used when an array identifier appear in function */
static bool isParameter(int offset){
	return (lastparaoffset!=NOPARA)&&(offset>=lastparaoffset);
}

/* procedure to generate code to check if an array index is a negative number or not */
static void checkArrayIndex(){
	emitRM("JGE",ac,1,pc,"check array index");
	emitRO("HALT",0,0,0,"array index is negative,program halted");
}

/* procedure to check function call in assign statement */
static void checkCallInAssign(TreeNode *decl,TreeNode *call){
	if((isCallInAssign)&&(strcmp(call->attr.name,"output")==0)){
		cout<<"function : "<<call->attr.name<<" ,cannot appeared in assignment,check line : "<<call->lineno<<endl;
		exit(1);
	}
	if((isCallInAssign)&&(decl->typekind==VOID)){
		cout<<"function : "<<call->attr.name<<" ,cannot appeared in assignment,check line : "<<call->lineno<<endl;
		exit(1);
	}
}

/* procedure to check function call in operation */
static void checkCallInOp(TreeNode *decl,TreeNode *call){
	if((isCallInOp)&&(strcmp(call->attr.name,"output")==0)){
		cout<<"function : "<<call->attr.name<<" ,cannot appeared as operand,check line : "<<call->lineno<<endl;
		exit(1);
	}
	if((isCallInOp)&&(decl->typekind==VOID)){
		cout<<"function : "<<call->attr.name<<" ,cannot appeared as operand,check line : "<<call->lineno<<endl;
		exit(1);
	}
}

/* procedure to allocate space for parameters */
static void allocateSpaceForParameter(TreeNode *t){
	if(t!=NULL){
		if(t->nodekind==ExpK){
			switch(t->subkind.exp){
				case IdE:{
					hashRecord *rec=symtabLookup(t->attr.name,currentScope);
					lastparaoffset=frameoffset;		// update last parameter's offset
					rec->offset=frameoffset--;	// allocate space for variable and update its information
					break;
				}
				
				case ArrE:{
					hashRecord *rec=symtabLookup(t->attr.name,currentScope);
					lastparaoffset=frameoffset;		// update last parameter's offset
					rec->offset=frameoffset--;	// allocate space for array's base address and update its information
					break;
				}
				
				default:
					break;
			}
		}
	}
}

/* procedure to allocate space for variable declaration */
static void allocateSpaceForVariable(TreeNode *t){
	if(t!=NULL){
		if(t->nodekind==ExpK){
			switch(t->subkind.exp){
				case IdE:{
					hashRecord *rec=symtabLookup(t->attr.name,currentScope);
					rec->offset=frameoffset--;	// allocate space for variable and update its information
					break;
				}
				
				case ArrE:{
					hashRecord *rec=symtabLookup(t->attr.name,currentScope);
					rec->offset=frameoffset--;	// allocate space for array and update its information
					int size=t->attr.val;
					frameoffset=frameoffset+1-size;					// update size of activation record
					break;
				}
				
				default:
					break;
			}
		}
	}
}

static void genFun(TreeNode *t){
	if(t!=NULL){
		switch(t->subkind.fun){
			case DeclF:{
				if(strcmp(t->attr.name,"main")==0){
					if(t->sibling!=NULL){
						cout<<"the last declaration must be main procedure,please check"<<endl;
						exit(1);
					}
					isMain=true;
					if(t->child[0]!=NULL){
						cout<<"main procedure cannot have parameter,check line : "<<t->lineno<<endl;
						exit(1);
					}
					jumpToMain();								// main procedure's location is known,so jump to it in the beginning
					initalizeFramePointer();		// initialize frame pointer before the execution of main procedure
				}
				hashRecord *rec=globolLookup(t->attr.name);
				if(rec==NULL)
					usedBeforeDeclaration(t->attr.name,t->lineno);
				rec->offset=emitLocation();		// record entry for function
				if(!isMain)
					emitRM("ST",ac,retFO,fp,"store return address in function");
				TreeNode *para=t->child[0];
				frameoffset=initFO;		// initialize frame offset
				lastparaoffset=NOPARA;		// initialize last parameter's offset
				while(para!=NULL){  // allocate space for function parameter in activation record
					allocateSpaceForParameter(para);
					para=para->sibling;
				}
				TreeNode *p=t->child[1];		// skip it's compound statement coz it's not a new scope
				TreeNode *localVar=p->child[0];
				while(localVar!=NULL){		// allocate space for local variable in activation record
					allocateSpaceForVariable(localVar);
					localVar=localVar->sibling;
				}
				cGen(p->child[1]);			// generate code for statement list
				if(isMain)
					emitRO("HALT",0,0,0,"done in main,program terminated");
				else
					emitRM("LD",pc,retFO,fp,"load return address in pc");
				break;
			}
			
			case CallF:{
				if(strcmp(t->attr.name,"input")==0){
					isArrayIdInCall=true;		// we are in function call
					checkArgForInput(t->child[0]);		
					emitRO("IN",ac,ac,ac,"input");
					isArrayIdInCall=false;		// we will leave function call soon
				}
				else if(strcmp(t->attr.name,"output")==0){
					isArrayIdInCall=true;		// we are in function call
					checkCallInAssign(t,t);
					checkCallInOp(t,t);
					checkArgForOutput(t->child[0]);
					cGen(t->child[0]);
					emitRO("OUT",ac,ac,ac,"output");
					isArrayIdInCall=false;		// we will leave function call soon
				}
				else{
					hashRecord *rec=globolLookup(t->attr.name);
					if(rec==NULL){
						cout<<"No such function declared,check name : "<<t->attr.name<<endl;
						exit(1);
					}
					isArrayIdInCall=true;		// we are in function call
					TreeNode *arg=t->child[0];
					TreeNode *fun=findDeclNode(funcTree,t->attr.name);
					TreeNode *para=fun->child[0];
					checkCallInAssign(fun,t);		// check if a function call in assign statement is legal
					checkCallInOp(fun,t);		// check if a function call in operation is legal
					int argindex=0;		// index to load argument in activation record correctly
					int oldoffset=frameoffset;		// save old frame offset for later restore
					while((para!=NULL)||(arg!=NULL)){		// load argument for new activation record
						checkArgPara(fun,arg,para);
						if((arg!=NULL)&&(arg->nodekind==FunK)&&(arg->subkind.fun==CallF))
							genFun(arg);
						else
							genExp(arg);
						emitRM("ST",ac,frameoffset--,fp,"push argument in temp location");
						--argindex;
						if(arg!=NULL)
							arg=arg->sibling;
						if(para!=NULL)
							para=para->sibling;
					}
					for(int i=0;i>argindex;i--){
						emitRM("LD",ac,oldoffset+i,fp,"load argument in ac");
						emitRM("ST",ac,frameoffset+initFO+i,fp,"send argument to function");
					}
					
					
					emitRM("ST",fp,frameoffset+ofpFO,fp,"store current frame pointer as control link");
					emitRM("LDA",fp,frameoffset,fp,"update frame pointer");
					emitRM("LDA",ac,1,pc,"load return address in ac");
					int entry=rec->offset;		// get function entry
					emitRM_Abs("LDA",pc,entry,"jump to function entry");
					emitRM("LD",fp,ofpFO,fp,"exit function,load control link in frame pointer");
					
					frameoffset=oldoffset;		// new activation record done,so temp location for argument is unnecessary,restore frame offset
						
					isArrayIdInCall=false;		// we will leave function call soon
				}
				break;
			}
			
			default:
				break;
		}
	}
}

static void genStmt(TreeNode *t){
	if(t!=NULL){
		switch(t->subkind.stmt){
			case ExpS:
				cGen(t->child[0]);
				break;
			
			case CompS:{
				TreeNode *localVar=t->child[0];
				int oldoffset=frameoffset;
				while(localVar!=NULL){		// allocate space for variable in activation record,which declared in compound statement
					allocateSpaceForVariable(localVar);
					localVar=localVar->sibling;
				}
				cGen(t->child[1]);		// generate code for statement list
				frameoffset=oldoffset;		// restore size of activation record
				break;
			}
			
			case IfS:{
				TreeNode *testpart=t->child[0];
				TreeNode *thenpart=t->child[1];
				TreeNode *elsepart=t->child[2];
				cGen(testpart);		// generate code for test part
				emitRM("JEQ",ac,2,pc,"check decision condition");
				emitRM("LDC",ac,1,ac,"true,load true value");
				emitRM("LDA",pc,2,pc,"if:jump to then");
				emitRM("LDC",ac,0,ac,"false,load false value");
				int savedLoc1=emitLocation();		// save location reserve for false jump
				int loc=emitSkip(1);		// skip a location
				
				cGen(thenpart);		// generate code for then part
				int savedLoc2=emitLocation();		// save location reserve for end jump
				loc=emitSkip(1);		// skip a location
				
				loc=emitLocation();		// get current location
			  emitBackpatch(savedLoc1);
				emitRM_Abs("LDA",pc,loc,"if:jump to else");
				emitRestore();
				cGen(elsepart);		// generate code for else part
				loc=emitLocation();		// get current location
				emitBackpatch(savedLoc2);
				emitRM_Abs("LDA",pc,loc,"then part done,jump to end");
				emitRestore();
				break;
			}
			
			case WhileS:{
				TreeNode *test=t->child[0];
				TreeNode *body=t->child[1];
				int savedLoc1=emitLocation();		// save location reserve for unconditional jump
				cGen(test);		// generate code for test part
				emitRM("JEQ",ac,2,pc,"check decision condition");
				emitRM("LDC",ac,1,ac,"true,load true value");
				emitRM("LDA",pc,2,pc,"while:jump to body");
				emitRM("LDC",ac,0,ac,"false,load false value");
				int savedLoc2=emitLocation();		// save location reserve for false jump
				int loc=emitSkip(1);
				
				cGen(body);		// generate code for body
				emitRM_Abs("LDA",pc,savedLoc1,"while:body end,unconditional jump to begin");
				
				loc=emitLocation();		// get current location
				emitBackpatch(savedLoc2);
				emitRM_Abs("LDA",pc,loc,"while:false jump to end");
				emitRestore();
				break;
			}
				
			case ReturnS:
				cGen(t->child[0]);
				if(isMain)
					emitRO("HALT",0,0,0,"return in main,program terminated");
				else
					emitRM("LD",pc,retFO,fp,"load return address in pc");				
				break;
				
			default:
				break;
		}
	}
}

static void genExp(TreeNode *t){
	if(t!=NULL){
		hashRecord *rec=NULL;
		int offset;
		switch(t->subkind.exp){
			case AssignE:{
				isCallInAssign=true;		// we are in assign statement
				TreeNode *arrayindex=t->child[0];
				TreeNode *rightvalue=t->child[1];
				rec=localLookup(t->attr.name,currentScope);		// check if the assigned variable is in local area
				if(rec!=NULL){		// the assigned variable is in local area
					offset=rec->offset;		// get its offset in current activation record
					if(arrayindex==NULL){		// if its a local variable
						if(rec->type==Array){
							cout<<"array address cannot be assigned,error line : "<<t->lineno<<endl;
							exit(1);
						}
						cGen(rightvalue);		// generate code for right value
						emitRM("ST",ac,offset,fp,"assign value to local variable");
					}
					else{		// if its an array's element
						cGen(arrayindex);		// generate code for array element's index
						checkArrayIndex();	// generate code to check array index
						if(isParameter(offset))		// if the array is an external reference
							emitRM("LD",ac1,offset,fp,"load array's base address in ac1");
						else		// otherwise the array is local array
							emitRM("LDA",ac1,offset,fp,"load local array's base address in ac1");
						emitRO("SUB",ac,ac1,ac,"load array element's address in ac");
						emitRM("ST",ac,frameoffset--,fp,"push array element's address to temp location");
						cGen(rightvalue);		// generate code for right value
						emitRM("LD",ac1,++frameoffset,fp,"load array element's address in ac1");
						emitRM("ST",ac,0,ac1,"assign value to array element");
					}
				}
				else{		// the assigned variable is in globol area
					rec=globolLookup(t->attr.name);
					if(rec==NULL)
						usedBeforeDeclaration(t->attr.name,t->lineno);
					if(rec->type==Function){
						cout<<"function : "<<t->attr.name<<" ,call without argument list,check line : "<<t->lineno<<endl;
						exit(1);
					}
					offset=rec->offset;		// get its offset in globol area
					if(arrayindex==NULL){	// if its a globol variable
						if(rec->type==Array){
							cout<<"array address cannot be assigned,check line : "<<t->lineno<<endl;
							exit(1);
						}
						cGen(rightvalue);		// generate code for right value
						emitRM("ST",ac,offset,gp,"assign value to globol variable");
					}
					else{		//if its a globol array's element
						cGen(arrayindex);		// generate code for array element's index
						checkArrayIndex();	// generate code to check array index
						emitRM("LDA",ac1,offset,gp,"load globol array's base address in ac1");
						emitRO("SUB",ac,ac1,ac,"load array element's address in ac");
						emitRM("ST",ac,frameoffset--,fp,"push array element's address to temp location");
						cGen(rightvalue);		// generate code for right value
						emitRM("LD",ac1,++frameoffset,fp,"load array element's address in ac1");
						emitRM("ST",ac,0,ac1,"assign value to globol array element");
					}
				}
				isCallInAssign=false;		// we weill leave assgin statement soon
				break;
			}
			
			case OpE:{
				isCallInOp=true;		// we are in operation
				TreeNode *left=t->child[0];		// left operand
				TreeNode *right=t->child[1];	// right operand
				cGen(left);
				emitRM("ST",ac,frameoffset--,fp,"push left operand to temp location");
				cGen(right);		// after this,right operand is in ac
				emitRM("LD",ac1,++frameoffset,fp,"load left operand in ac1");
				switch(t->attr.op){
					case PLUS:
						emitRO("ADD",ac,ac1,ac,"add");
						break;
					case MINUS:
						emitRO("SUB",ac,ac1,ac,"sub");
						break;
					case TIMES:
						emitRO("MUL",ac,ac1,ac,"mul");
						break;
					case OVER:
						emitRO("DIV",ac,ac1,ac,"div");
						break;
					case LT:
						emitRO("SUB",ac,ac1,ac,"sub");
						emitRM("JLT",ac,2,pc,"less than");
						emitRM("LDC",ac,0,ac,"false,load false value");
						emitRM("LDA",pc,1,pc,"jump");
						emitRM("LDC",ac,1,ac,"true,load true value");
						break;
					case LE:
						emitRO("SUB",ac,ac1,ac,"sub");
						emitRM("JLE",ac,2,pc,"less equal than");
						emitRM("LDC",ac,0,ac,"false,load false value");
						emitRM("LDA",pc,1,pc,"jump");
						emitRM("LDC",ac,1,ac,"true,load true value");
						break;
					case GT:
						emitRO("SUB",ac,ac1,ac,"sub");
						emitRM("JGT",ac,2,pc,"greater than");
						emitRM("LDC",ac,0,ac,"false,load false value");
						emitRM("LDA",pc,1,pc,"jump");
						emitRM("LDC",ac,1,ac,"true,load true value");
						break;
					case GE:
						emitRO("SUB",ac,ac1,ac,"sub");
						emitRM("JGE",ac,2,pc,"greater equal than");
						emitRM("LDC",ac,0,ac,"false,load false value");
						emitRM("LDA",pc,1,pc,"jump");
						emitRM("LDC",ac,1,ac,"true,load true value");
						break;
					case EQ:
						emitRO("SUB",ac,ac1,ac,"sub");
						emitRM("JEQ",ac,2,pc,"equal");
						emitRM("LDC",ac,0,ac,"false,load false value");
						emitRM("LDA",pc,1,pc,"jump");
						emitRM("LDC",ac,1,ac,"true,load true value");
						break;
					case NE:
						emitRO("SUB",ac,ac1,ac,"sub");
						emitRM("JNE",ac,2,pc,"not equal");
						emitRM("LDC",ac,0,ac,"false,load false value");
						emitRM("LDA",pc,1,pc,"jump");
						emitRM("LDC",ac,1,ac,"true,load true value");
						break;
					default:
						break;
				}
				isCallInOp=false;		// we will leave operation soon
				break;
			}
			
			case ConstE:{
				int cnum=t->attr.val;
				emitRM("LDC",ac,cnum,ac,"load const");
				break;
			}
			
			case IdE:{
				rec=localLookup(t->attr.name,currentScope);
				if(rec!=NULL){		// if identifier is in local area
					offset=rec->offset;
					if((rec->type==Array)&&(!isArrayIdInCall)){
						cout<<"single array identifier can appear only in function call,error line : "<<t->lineno<<endl;
						exit(1);
					}
					if(rec->type==Array){		// if its a single array identifier
						if(isParameter(rec->offset))		// if the array is an external reference
							emitRM("LD",ac,offset,fp,"load array's base address in ac");
						else		// otherwose the array is a local array
							emitRM("LDA",ac,offset,fp,"load local array's base address in ac");
					}
					else
						emitRM("LD",ac,offset,fp,"load local variable in ac");
				}
				else{		// if identifier is in globol area
					rec=globolLookup(t->attr.name);
					if(rec==NULL)
						usedBeforeDeclaration(t->attr.name,t->lineno);
					if(rec->type==Function){
						cout<<"function : "<<t->attr.name<<" ,call without argument list,check line : "<<t->lineno<<endl;
						exit(1);
					}
					offset=rec->offset;
					if((rec->type==Array)&&(!isArrayIdInCall)){
						cout<<"single array identifier can appear only in function call,error line : "<<t->lineno<<endl;
						exit(1);
					}
					if(rec->type==Array)		// if its a single array identifier
						emitRM("LDA",ac,offset,gp,"load globol array's base address in ac");
					else
						emitRM("LD",ac,offset,gp,"load globol variable in ac");
				}
				break;
			}
				
			case ArrE:{
				TreeNode *arrayindex=t->child[0];
				rec=localLookup(t->attr.name,currentScope);
				if(rec!=NULL){		// if the array is in local area
					offset=rec->offset;
					if(isParameter(offset))		// if the array is an external reference
						emitRM("LD",ac1,offset,fp,"load array's base address in ac1");
					else		// otherwose the array is a local array
						emitRM("LDA",ac1,offset,fp,"load local array's base address in ac1");
				}
				else{		// if array is in globol area
					rec=globolLookup(t->attr.name);
					if(rec==NULL)
						usedBeforeDeclaration(t->attr.name,t->lineno);
					offset=rec->offset;
					emitRM("LDA",ac1,offset,gp,"load globol array's base address in ac1");
				}
				emitRM("ST",ac1,frameoffset--,fp,"push array's base address to temp location");
				cGen(arrayindex);		// generate code for array element's index
				checkArrayIndex();		// generate code to check array index
				emitRM("LD",ac1,++frameoffset,fp,"load array's base address in ac1");
				emitRO("SUB",ac,ac1,ac,"load array element's address in ac");
				emitRM("LD",ac,0,ac,"load array element's value in ac");
				break;
			}
			
			default:
				break;
		}
	}
}

/* the index of compound's record in symbol table */
static int compoundIndex=-1;
static void cGen(TreeNode *t){
	if(t!=NULL){
		switch(t->nodekind){
			case FunK:
				if(t->subkind.fun==DeclF){
					compoundIndex=-1;
					hashRecord *funRec=symtabLookup(t->attr.name,currentScope);
					currentScope=funRec->nextTab;
				}
				genFun(t);
				if(t->subkind.fun==DeclF){
					compoundIndex=-1;
					currentScope=currentScope->preLayer;
				}
				break;
			
			case StmtK:
				int oldIndex;
				if(t->subkind.stmt==CompS){
					++compoundIndex;
					oldIndex=compoundIndex;
					hashRecord *compRec=symtabLookup(compoundIndex,currentScope);
					//cout<<"Enter compound scope,compound index : "<<compoundIndex<<endl;
					currentScope=compRec->nextTab;
					compoundIndex=-1;
				}
				genStmt(t);
				if(t->subkind.stmt==CompS){
					currentScope=currentScope->preLayer;
					compoundIndex=oldIndex;
					//cout<<"Exit compound scope,compound index : "<<compoundIndex<<endl;
				}
				break;
		
			case ExpK:
				if(currentScope==globolScope){
					allocateGlobolArea(t);		// allocate space for globol variable and array
				}
				else{
					genExp(t);
				}
				break;
			
			default:
				break;
		}
		cGen(t->sibling);
	}
}

/* function to generate code from syntax tree */
void codeGen(TreeNode *syntaxTree,char *filename){
	code<<"* code generated for source file : "<<filename<<endl<<endl;
	globolScope=globolSymtab;
	currentScope=globolSymtab;
	standardPrelude();		// standard prelude for runtime environment
	saveForMain();				// jump to main procedure to execute
	cGen(syntaxTree);
}
