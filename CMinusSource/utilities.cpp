/* C Minus utilities - definition */
#include "utilities.h"

extern const int CHILD;
static int indentno=0;	// for indention

void printToken(TokenType token,char *lexeme,int lineno){
	info<<"\t"<<"("<<lineno<<")"<<" ";
	switch(token){
		case ELSE:case IF:case RETURN:case WHILE:
			info<<lexeme<<" : "<<"reserved word"<<endl;
			break;
		case INT:case VOID:
			info<<lexeme<<" : "<<"type"<<endl;
			break;
		case PLUS:
			info<<"+"<<" : "<<"arithmetic operator +"<<endl;
			break;
		case MINUS:
			info<<"-"<<" : "<<"arithmetic operator -"<<endl;
			break;
		case TIMES:
			info<<"*"<<" : "<<"arithmetic operator *"<<endl;
			break;
		case OVER:
			info<<"/"<<" : "<<"arithmetic operator /"<<endl;
			break;
		case LT:
			info<<"<"<<" : "<<"comparision operator: less than"<<endl;
			break;
		case LE:
			info<<"<="<<" : "<<"comparision operator: less or equal"<<endl;
			break;
		case GT:
			info<<">"<<" : "<<"comparision operator: greater than"<<endl;
			break;
		case GE:
			info<<">="<<" : "<<"comparision operator: greater or equal"<<endl;
			break;
		case EQ:
			info<<"=="<<" : "<<"comparision operator: equal"<<endl;
			break;
		case NE:
			info<<"!="<<" : "<<"comparision operator: not equal"<<endl;
			break;
		case ASSIGN:
			info<<"="<<" : "<<"assignment operator"<<endl;
			break;
		case SIMI:
			info<<";"<<" : "<<"simicolon operator"<<endl;
			break;
		case COMMA:
			info<<","<<" : "<<"comma operator"<<endl;
			break;
		case LP:
			info<<"("<<" : "<<"left paren"<<endl;
			break;
		case RP:
			info<<")"<<" : "<<"right paren"<<endl;
			break;
		case LSB:
			info<<"["<<" : "<<"left square bracket"<<endl;
			break;
		case RSB:
			info<<"]"<<" : "<<"right square bracket"<<endl;
			break;
		case LCB:
			info<<"{"<<" : "<<"left curly bracket"<<endl;
			break;
		case RCB:
			info<<"}"<<" : "<<"right curly bracket"<<endl;
			break;
		case ID:
			info<<lexeme<<" : "<<"identifier"<<endl;
			break;
		case NUM:
			info<<lexeme<<" : "<<"const"<<endl;
			break;
		case ENDFILE:
			info<<"END"<<endl;
			break;
		case UNKNOWN:
			info<<lexeme<<" : "<<"Unknown token"<<endl;
			break;
		default:
			info<<"not even a token"<<endl;
			break;
	}
}

/* Function to copy a string to tree node attribute */
void copyString(TreeNode *t,char *str){
	int len=strlen(str)+1;
	t->attr.name=new char[len+1];
	strcpy(t->attr.name,str);
}

/* Function to create a new function node */
TreeNode* getFunNode(FunKind kind,TokenType type){
	TreeNode *t=NULL;
	t=new TreeNode;
	t->nodekind=FunK;
	if(kind==DeclF)
		t->typekind=type;
	t->lineno=lineNo;
	t->subkind.fun=kind;
	copyString(t,tokenLexeme);
	for(int i=0;i<CHILD;i++)
		t->child[i]=NULL;
	t->sibling=NULL;
	/*if(t!=NULL){
		switch(kind){
			case DeclK:
				info<<"Done Decl getFunNode"<<endl;
				break;
			case CallK:
				info<<"Done Call getFunNode"<<endl;
				break;
		}
	}
	else
		info<<"Wrong getFunNode"<<endl;*/
	return t;
}

/* Function to create a new statement node */
TreeNode* getStmtNode(StmtKind kind){
	TreeNode *t=NULL;
	t=new TreeNode;
	t->nodekind=StmtK;
	t->lineno=lineNo;
	t->subkind.stmt=kind;
	for(int i=0;i<CHILD;i++)
		t->child[i]=NULL;
	t->sibling=NULL;
	/*
	if(t!=NULL){
		switch(kind){
			case IfK:
				info<<"Done If getStmtNode"<<endl;
				break;
			case WhileK:
				info<<"Done While getStmtNode"<<endl;
				break;
			case ReturnK:
				info<<"Done Return getStmtNode"<<endl;
				break;
		}
	}
	else
		info<<"Wrong getStmtNode"<<endl;*/
	return t;
}

/* Function to create a new expression node */
TreeNode* getExpNode(ExpKind kind,TokenType tok){
	TreeNode *t=NULL;
	t=new TreeNode;
	t->nodekind=ExpK;
	t->lineno=lineNo;
	t->subkind.exp=kind;
	switch(kind){
		case AssignE:
			copyString(t,tokenLexeme);
			break;
		case OpE:
			t->attr.op=tok;
			break;
		case ConstE:
			t->attr.val=atoi(tokenLexeme);
			break;
		case IdE:
			t->typekind=tok;
			if(tok==VOID){
				cout<<"variable cannot be void type,in line: "<<t->lineno<<endl;
				exit(1);
			}
			copyString(t,tokenLexeme);
			break;
		case ArrE:
			t->typekind=tok;
			if(tok==VOID){
				cout<<"variable cannot be void type,in line: "<<t->lineno<<endl;
				exit(1);
			}
			copyString(t,tokenLexeme);
			break;
		default:
			break;
	}
	for(int i=0;i<CHILD;i++)
		t->child[i]=NULL;
	t->sibling=NULL;
	/*if(t!=NULL){
		switch(kind){
			case AssignK:
				info<<"Done Assign getExpNode"<<endl;
				break;
			case OpK:
				info<<"Done Op getExpNode"<<endl;
				break;
			case ConstK:
				info<<"Done Const getExpNode"<<endl;
				break;
			case IdK:
				info<<"Done ID getExpNode"<<endl;
				break;
			case ArrK:
				info<<"Done Array getExpNode"<<endl;
				break;
		}
		//info<<"Done getExpNode"<<kind<<endl;
	}
	else
		info<<"Wrong getExpNode"<<endl;*/
	return t;
}

static void printSpaces(){
	for(int i=0;i<indentno;i++)
		info<<" ";
}

/* Procedure to print syntax tree in indention formation */
void printTree(TreeNode *tree){
	if(tree!=NULL){
		indentno+=2;
		printSpaces();
		switch(tree->nodekind){
			case FunK:
				switch(tree->subkind.fun){
					case DeclF:
						info<<"Function declaration:"<<tree->attr.name<<" return type:"<<tree->typekind<<endl;
						break;
					case CallF:
						info<<"Function call:"<<tree->attr.name<<" return type:"<<tree->typekind<<endl;
						break;
					default:
						info<<"Unknown function kind"<<endl;
						break;
				}
				break;
			case StmtK:
				switch(tree->subkind.stmt){
					case ExpS:
						info<<"Expression:"<<endl;
						break;
					case CompS:
						info<<"Compound:"<<endl;
						break;
					case IfS:
						info<<"If:"<<endl;
						break;
					case WhileS:
						info<<"While:"<<endl;
						break;
					case ReturnS:
						info<<"Return:"<<endl;
						break;
					default:
						info<<"Unknown statement kind"<<endl;
						break;
				}
				break;
			case ExpK:
				switch(tree->subkind.exp){
					case AssignE:
						info<<"Assign to: "<<tree->attr.name<<endl;
						break;
					case OpE:
						info<<"Operator: "<<tree->attr.op<<endl;
						break;
					case ConstE:
						info<<"Const: "<<tree->attr.val<<endl;
						break;
					case IdE:
						info<<"Identifier: "<<tree->attr.name<<endl;
						break;
					case ArrE:
						info<<"Array: "<<tree->attr.name<<" Index value:"<<tree->attr.val<<endl;
						break;
					default:
						info<<"Unknown expression kind"<<endl;
						break;
				}
				break;
			default:
				info<<"Unknown situation";
				break;
		}
		for(int i=0;i<CHILD;i++)
			printTree(tree->child[i]);
		indentno-=2;
		printTree(tree->sibling);
	}
}

/* Procedure to delete syntax tree after code generation */
void destroyTree(TreeNode *tree){
	if(tree!=NULL){
		destroyTree(tree->sibling);
		for(int i=0;i<CHILD;i++)
			destroyTree(tree->child[i]);
		delete tree;
	}
}	
