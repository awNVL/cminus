/* C Minus symbol table - definition */
#include "symbol.h"

symTable *globolSymtab=NULL;				// globol symbol table

/* hash function */
int hash(char *key){
	int i=0;
	int value=0;
	while(key[i]!='\0'){
		value=((value<<SHIFT)+static_cast<int>(key[i]))%SIZE;
		++i;
	}
	return value;
}

/* Function to insert a record to symbol table */
symTable* symtabInsert(char *id,TypeKind type,symTable *symtab){
	/* case for a compound node */
	if(id==NULL){
		for(int i=0;i<SIZE;i++){
			/* we have record on this link,so search if there is a compound hash record exist */
			if((symtab->table[i])!=NULL){
				bool isCompound=false;
				hashRecord *rec=symtab->table[i];
				while(rec!=NULL){
					if(rec->name==NULL){
						isCompound=true;
						break;
					}
					rec=rec->next;
				}
				if(isCompound)	// a compound hash record already exist in this link,so find next link in symbol table
					continue;
				else{						// otherwise insert a compound hash record in this link
					rec=new hashRecord;
					rec->name=NULL;
					rec->type=type;
					rec->next=symtab->table[i];
					symtab->table[i]=rec;
					symTable *compoundSymtab=new symTable;
					rec->nextTab=compoundSymtab;				// create a symbol table for this compound's scope
					compoundSymtab->preLayer=symtab;		// and link it to current symbol table
					for(int i=0;i<SIZE;i++)
						compoundSymtab->table[i]=NULL;
					return compoundSymtab;
					break;
				}
			}
			/* we are in a position which has no record,so insert a compound hash record here */
			else{
				hashRecord *rec=new hashRecord;
				rec->name=NULL;
				rec->type=type;
				rec->next=symtab->table[i];
				symtab->table[i]=rec;
				symTable *compoundSymtab=new symTable;
				rec->nextTab=compoundSymtab;				// create a symbol table for this compound's scope
				compoundSymtab->preLayer=symtab;		// and link it to current symbol table
				for(int i=0;i<SIZE;i++)
					compoundSymtab->table[i]=NULL;
				return compoundSymtab;
				break;
			}
		}
	}
	/* case for a non-compound record */ 
	else{
		int h=hash(id);
		hashRecord *rec=symtab->table[h];
		/* search if the identifier already exist */
		while(rec!=NULL){
			if(strcmp(rec->name,id)==0)		// if it already exist,do nothing
				return symtab;
			rec=rec->next;
		}
		/* if we didn't find it,then insert this record */
		rec=new hashRecord;
		rec->name=new char[strlen(id)+1];
		strcpy(rec->name,id);
		rec->type=type;
		rec->next=symtab->table[h];
		symtab->table[h]=rec;
		/* case for function-declaration record */
		if(type==Function){
			symTable *funSymtab=new symTable;
			rec->nextTab=funSymtab;							// create a symbol table for this function's scope
			funSymtab->preLayer=symtab;					// and link it to current symbol table
			for(int i=0;i<SIZE;i++)
				funSymtab->table[i]=NULL;
			return funSymtab;
		}
		/* case for common variable's record */
		else{
			rec->nextTab=NULL;
			return symtab;
		}
	}
}

/* Function to look up a record in symbol table */
hashRecord* symtabLookup(char *id,symTable *symtab){
	/* Until we are out of globol table */
	while(symtab!=NULL){
		int h=hash(id);
		hashRecord *rec=symtab->table[h];
		while(rec!=NULL){
			if((rec->name!=NULL)&&(strcmp(rec->name,id)==0))		// the first condition make sure we will skip a compound hash record
				return rec;																				// otherwise a reference of empty pointer will cause fatal error
			rec=rec->next;
		}
		/* we didn't find in this symbol table,so go to the preceding symbol table */
		symtab=symtab->preLayer;
	}
	if(symtab==NULL){
		cout<<"Used before declaration: "<<id<<endl;
		exit(1);
	}
}

/* Function to look up a record in symbol table for compound statement */
hashRecord* symtabLookup(int index,symTable *symtab){
	/* Search compound statement only under current symbol table */
	if(symtab!=NULL){
		hashRecord *rec=symtab->table[index];
		while(rec!=NULL){
			if(rec->name==NULL)
				return rec;
			rec=rec->next;
		}
	}
	else{
		cout<<"symbol table empty"<<endl;
		exit(1);
	}
}

/* Function to look up a record in local symbol table */
hashRecord *localLookup(char *id,symTable *symtab){
	hashRecord *rec=NULL;
	while(symtab!=globolSymtab){
		int h=hash(id);
		rec=symtab->table[h];
		while(rec!=NULL){
			if((rec->name!=NULL)&&(strcmp(rec->name,id)==0))
				return rec;
			rec=rec->next;
		}
		/* we didn't find in this symbol table,so go to the preceding symbol table */
		symtab=symtab->preLayer;
	}
	return rec;
}

/* Function to look up a record in globol symbol table */
hashRecord* globolLookup(char *id){
	int h=hash(id);
	hashRecord *rec=globolSymtab->table[h];
	while(rec!=NULL){
		if((rec->name!=NULL)&&(strcmp(rec->name,id)==0))
			return rec;
		rec=rec->next;
	}
	return rec;
}

/* Function to add predefined function in globol symbol table */
void addPredefined(){
	char input[]="input";
	char output[]="output";
	symTable *temptab=symtabInsert(input,Function,globolSymtab);
	temptab=symtabInsert(output,Function,globolSymtab);
}
