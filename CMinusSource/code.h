/* C Minus code generation utilities - header */
#ifndef CODE_H
#define CODE_H
#include "globals.h"
#include "symbol.h"

extern int currentLoc;		// the current location to emit
extern int highestLoc;			// the highest emited location

/* name of register */
const int ac=0;		// accumulator
const int ac1=1;	// accumulator
const int gp=5;		// globol pointer
const int fp=6;		// frame pointer
const int pc=7;		// program counter


/* function to emit register-only instruction */
void emitRO(char *opcode,int r,int s,int t,char *c);

/* function emit register-memory instruction */
void emitRM(char *opcode,int r,int d,int t,char *c);

/* function to convert absolute address to register-relative address,take pc as target register */
void emitRM_Abs(char *opcode,int r,int d,char *c);

/* function to skip specific number of instruction location */
int emitSkip(int num);

/* function to set current instruction location as backpatch instruction location */
void emitBackpatch(int loc);

/* function to restore current instruction location after backpatching */
void emitRestore();

/* function to return current location */
int emitLocation();
#endif
