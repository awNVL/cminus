/* C Minus scanner - definition */
#include <cstring>
#include "globals.h"
#include "utilities.h"
#include "scanner.h"

/* variabls and constants to handle a line of source file */
static const int LINESIZE=256;			// max size of a line
static char lineBuf[LINESIZE];		// to read a line from source file
static int lineLen=0;							// length of a line buffer
int lineNo=0;											// record how many lines we have read from source file
static int linePos=0;							// current position in a line buffer
static char ch;										// character of current position

char tokenLexeme[TOKENLEN];			// record token string value of current token
const int WIDTH=4;							// the width of a token and a line number in the token file

static const int RESERVEDWORDS=6;
/* to check if an identifier is a reserved word */
static char *reservedTable[RESERVEDWORDS]={"else","if","int","return","void","while"};
static TokenType reservedType[RESERVEDWORDS]={ELSE,IF,INT,RETURN,VOID,WHILE};

static void getChar(){
	if(!source.eof()){
		if(linePos>=lineLen){
			source.getline(lineBuf,LINESIZE);
			++lineNo;
			lineLen=strlen(lineBuf);
			lineBuf[lineLen++]='\n';
			lineBuf[lineLen]='\0';
			linePos=0;
			info<<lineNo<<": "<<lineBuf;
		}
		ch=lineBuf[linePos++];
	}
	else if(linePos<lineLen)		// we get last line of source but the read haven't finish yet
		ch=lineBuf[linePos++];
	else
		ch=EOF;
}

/* non-consume situation */
static void ungetChar(){
	ch=lineBuf[--linePos];
}

static bool isWhiteSpace(){
	return (ch==' ')||(ch=='\n')||(ch=='\t');
}

/* check if next two characters are left comment tokenn */
static bool isLeftComment(){
	return (ch=='/')&&(lineBuf[linePos]=='*');		// take care current position is next character of ch
}

/* check if next two characters are right comment token */
static bool isRightComment(){
	return (ch=='*')&&(lineBuf[linePos]=='/');		// take care current position is next character of ch
}

TokenType getToken(){
	StateType state=Sstart;
	TokenType token;
	int len=0;
	while((state!=Sdone)&&(state!=Serror)){
		getChar();
		switch(state){
			case Sstart:
				if(isWhiteSpace())					// skip white space in start state
					;		// getChar() is used outside
				else if(isLeftComment()){		// skip comment in start state
					getChar();		// after this character is *
					state=Scomment;
				}
				else if(ch=='\0')		// when in a blank line no token should be stored
					;
				else if(ch==EOF){		// end of file,we are done
					state=Sdone;
					token=ENDFILE;
				}
				else if(isalpha(ch)){
					state=Sid;
					tokenLexeme[len++]=ch;
				}
				else if(isdigit(ch)){
					state=Snum;
					tokenLexeme[len++]=ch;
				}
				else if(ch=='+'){
					state=Sdone;
					token=PLUS;
				}
				else if(ch=='-'){
					state=Sdone;
					token=MINUS;
				}
				else if(ch=='*'){
					state=Sdone;
					token=TIMES;
				}
				else if(ch=='/'){
					state=Sdone;
					token=OVER;
				}
				else if(ch=='<'){		// maybe operator < or operator <=
					state=Sle;
				}
				else if(ch=='>'){		// maybe operator > or operator >=
					state=Sge;
				}
				else if(ch=='='){		// maybe operator = or operator ==
					state=Seq;
				}
				else if(ch=='!'){
					state=Sne;
				}
				else if(ch==';'){
					state=Sdone;
					token=SIMI;
				}
				else if(ch==','){
					state=Sdone;
					token=COMMA;
				}
				else if(ch=='('){
					state=Sdone;
					token=LP;
				}
				else if(ch==')'){
					state=Sdone;
					token=RP;
				}
				else if(ch=='['){
					state=Sdone;
					token=LSB;
				}
				else if(ch==']'){
					state=Sdone;
					token=RSB;
				}
				else if(ch=='{'){
					state=Sdone;
					token=LCB;
				}
				else if(ch=='}'){
					state=Sdone;
					token=RCB;
				}
				else{
					state=Serror;
					token=UNKNOWN;
					tokenLexeme[len++]=ch;
				}
				break;
			
			case Scomment:
				if(!isRightComment())
					;		// getChar() is used outside
				else{
					getChar();		// after this character is /
					state=Sstart;
				}
				break;
					
			case Sid:
				if(isalpha(ch))
					tokenLexeme[len++]=ch;
				else{
					ungetChar();
					state=Sdone;
					token=ID;
				}
				break;
			
			case Snum:
				if(isdigit(ch))
					tokenLexeme[len++]=ch;
				else{
					ungetChar();
					state=Sdone;
					token=NUM;
				}
				break;
			
			case Sle:		// for case operator < and operator <=
				if(ch=='='){
					state=Sdone;
					token=LE;
				}
				else{
					ungetChar();
					state=Sdone;
					token=LT;
				}
				break;
				
			case Sge:		// for case operator > and operator >=
				if(ch=='='){
					state=Sdone;
					token=GE;
				}
				else{
					ungetChar();
					state=Sdone;
					token=GT;
				}
				break;
				
			case Seq:		// for case operator = and operator ==
				if(ch=='='){
					state=Sdone;
					token=EQ;
				}
				else{
					ungetChar();
					state=Sdone;
					token=ASSIGN;
				}
				break;
			
			case Sne:
				if(ch=='='){
					state=Sdone;
					token=NE;
				}
				else{
					state=Serror;
					token=UNKNOWN;
				}
				break;
				
			default:
				state=Serror;
				token=UNKNOWN;
				tokenLexeme[len++]=ch;
				break;
		}
	}
	if((state==Sdone)||(state==Serror)){
		tokenLexeme[len]='\0';
		for(int i=0;i<RESERVEDWORDS;i++)
			if(strcmp(tokenLexeme,reservedTable[i])==0)
				token=reservedType[i];
		printToken(token,tokenLexeme,lineNo);
		temp<<setw(WIDTH)<<token;
		temp<<setw(WIDTH)<<lineNo;
		if((token==ID)||(token==NUM))
			temp<<setw(TOKENLEN)<<tokenLexeme;
		else 
			temp<<setw(TOKENLEN)<<" ";
	}
	return token;
}
