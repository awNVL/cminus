/* C Minus parser - header */
#ifndef PARSER_H
#define PARSER_H
#include "utilities.h"

/* Function to get a token from token file and can easily *
 * back up,to provide a interface for further process     */
TokenType currentToken();

/* Function to get previous token without moving *
 * current file position,used for token back up  */           
TokenType readPrevToken();

/* Get current token's position in file */
int getPos();

/* Procedure to move current token back up once */
void tokenBackup();

/* Procedure to move current token back up to specified *
 * position,used to handle really speacial case         */
void tokenBackup(int pos);

/* Function to construct a syntax tree */
TreeNode* parse();
#endif
