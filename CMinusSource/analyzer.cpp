/* C Minus semantic analyzer - definition */
#include "analyzer.h"

static int indentNo=0;	// for indention

/* Variables used when traverse the syntax tree */
symTable *currentSymtab;			// holds current symbol table
TreeNode *currentNode;				// holds current node
TreeNode *parentNode=NULL;		// holds current node's parent node

/* Procedure to build symbol table */
void buildSymTab(TreeNode *t){
	if(t!=NULL){
		currentNode=t;
		bool enterNewScope=false;
		switch(t->nodekind){
			case FunK:
				switch(t->subkind.fun){
					case DeclF:
						/* Insert a function-declaration record and update current symbol tale */
						enterNewScope=true;
						currentSymtab=symtabInsert(t->attr.name,Function,currentSymtab);
						break;
					default:
						break;
				}
				break;
				
			case StmtK:
				switch(t->subkind.stmt){
					case CompS:
						/* Insert a compound record and update current symbol tale */
						if((parentNode->nodekind==FunK)&&(parentNode->subkind.fun==DeclF))
							;
						else{
							enterNewScope=true;
							currentSymtab=symtabInsert(NULL,Void,currentSymtab);
						}
						break;
					default:
						break;
				}
				break;
				
			case ExpK:
				switch(t->subkind.exp){
					case IdE:case ArrE:
						/* if it's a globol variable,insert it to symbol table,this condition should be  */
						/* check first,coz a reference to a empty pointer can lead to disastrous result  */
						if(parentNode==NULL){
							TypeKind type;
							if(t->subkind.exp==ArrE)
								type=Array;
							else
								type=Integer;
							currentSymtab=symtabInsert(t->attr.name,type,currentSymtab);
						}
						/* if it's a parameter-declaration node or a local variable-declaration node,insert it to symbol table */
						else if(((parentNode->nodekind==StmtK)&&(parentNode->subkind.stmt==CompS)&&(currentNode==parentNode->child[0]))
							||((parentNode->nodekind==FunK)&&(parentNode->subkind.fun==DeclF)&&(currentNode==parentNode->child[0]))){
							TreeNode *paraNode=currentNode;
							while(paraNode!=NULL){
								TypeKind type;
								if(paraNode->subkind.exp==ArrE)
									type=Array;
								else
									type=Integer;
								currentSymtab=symtabInsert(paraNode->attr.name,type,currentSymtab);
								paraNode=paraNode->sibling;
							}
							return;				// parameter list insertion done
						}
						/* otherwise do nothing */
						else
							;
						break;
						
					default:
						break;
				}
				break;
				
			default:
				break;
		}
		TreeNode *oldparent=parentNode;
		parentNode=t;
		for(int i=0;i<CHILD;i++)
			buildSymTab(t->child[i]);
		parentNode=oldparent;												// restore parent node before enter current node's sibling node 
		if(enterNewScope)
			currentSymtab=currentSymtab->preLayer;		// restore the symbol table
		buildSymTab(t->sibling);										// coz current node and it's sibling node are in the same scope
	}
}

/* Check if return statement's return type match it's function-declaration return type */
static int returnCount=0;
static void checkReturnType(TreeNode *t,TypeKind funType){
	if(t==NULL)
		return;
	/* case for return statement node */
	if((t->nodekind==StmtK)&&(t->subkind.stmt==ReturnS)){
		/* count how many times we met return node */
		++returnCount;
		TypeKind returnType;
		if(t->child[0]==NULL)
			returnType=Void;
		else
			returnType=Integer;
		if(funType!=returnType){
			cout<<"Incorrect function return type,in line: "<<t->lineno<<endl;
			exit(1);
		}
		for(int i=0;i<CHILD;i++)
			checkReturnType(t->child[i],funType);
		checkReturnType(t->sibling,funType);
	}
	/* all kinds of node need to check */
	for(int i=0;i<CHILD;i++)
		checkReturnType(t->child[i],funType);
	checkReturnType(t->sibling,funType);
}

/* Procedure to check and set type information */
void typeCheck(TreeNode *t){
	if(t!=NULL){
		for(int i=0;i<CHILD;i++)
			typeCheck(t->child[i]);
		typeCheck(t->sibling);
		switch(t->nodekind){
			case FunK:
				switch(t->subkind.fun){
					case DeclF:
						t->type=Function;
						TypeKind returnType;
						if(t->typekind==INT)
							returnType=Integer;
						else
							returnType=Void;
						/* take care the return statement is in it's compound node's subnode */
						returnCount=0;
						checkReturnType(t->child[1],returnType);
						if((returnType==Integer)&&(returnCount==0)){
							cout<<t->attr.name<<" function don't have return value,in line: "<<t->lineno<<endl;
							exit(1);
						}
						break;
					case CallF:
						t->type=Function;
						break;
						
					default:
						break;
				}
				break;
				
			case StmtK:
				switch(t->subkind.stmt){
					case ReturnS:
						if(t->child[0]==NULL)
							t->type=Void;
						else
							t->type=Integer;
						break;
					
					default:
						break;
				}
				break;
			
			case ExpK:
				switch(t->subkind.exp){
					case IdE:
						t->type=Integer;
						break;
					
					case ArrE:
						t->type=Array;
						break;
					
					case OpE:
						t->type=Integer;
						break;
						
					case AssignE:
						t->type=Integer;
						break;
					
					case ConstE:
						t->type=Integer;
					
					default:
						break;
				}
				break;
				
			default:
				break;
		}
	}
}

/* Procedure use type check as internal recursive */
void typeChecking(TreeNode *syntaxTree){
	typeCheck(syntaxTree);
	info<<endl<<endl<<"Type checking done"<<endl;
}

static void indentSpaces(){
	for(int i=0;i<indentNo;i++)
		info<<" ";
}

void printSymbolTable(symTable *symtab){
	if(symtab!=NULL){
		indentNo+=2;
		for(int i=0;i<SIZE;i++){
			if(symtab->table[i]!=NULL){
				hashRecord *rec=symtab->table[i];
				while(rec!=NULL){
					indentSpaces();
					if(rec->name==NULL){
						info<<"compound scope:"<<endl;
						printSymbolTable(rec->nextTab);
						indentNo-=2;
					}
					else if((rec->type==Function)&&(strcmp(rec->name,"input")!=0)&&(strcmp(rec->name,"output")!=0)){
						info<<rec->name<<" function scope:"<<endl;
						printSymbolTable(rec->nextTab);
						indentNo-=2;
					}
					else{
						info<<rec->name<<endl;
					}
					rec=rec->next;
				}
			}
		}
	}
}

/* Procedure to include build symbol table and print symbol table */
void buildSymbolTable(TreeNode *syntaxTree){
	/* Create globol symbol table and initialize it */
	globolSymtab=new symTable;
	globolSymtab->preLayer=NULL;
	for(int i=0;i<SIZE;i++)
		globolSymtab->table[i]=NULL;
	currentSymtab=globolSymtab;
	parentNode=NULL;		// initialize parent node,indicate now we are in globol symbol table
	addPredefined();		// add predefined function in globol symbol table
	buildSymTab(syntaxTree);
	info<<endl<<"Symbol table"<<endl<<endl<<"globol scope:"<<endl;
	printSymbolTable(globolSymtab);
}
