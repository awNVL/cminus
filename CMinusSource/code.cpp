/* C Minus code generation utilities - definition */
#include "code.h"
int currentLoc=0;		// the current location to emit
int highestLoc;			// the highest emited location
const int LOCWIDTH=4;
const int CODEWIDTH=6;
const int NUMWIDTH=6;
const int SPACEWIDTH=6;
const int COMMENTWIDTH=20;

/* function to emit register-only instruction */
void emitRO(char *opcode,int r,int s,int t,char *c){
	code<<setw(LOCWIDTH)<<currentLoc++<<":"<<setw(CODEWIDTH)<<opcode<<setw(NUMWIDTH)<<r<<","<<s<<","<<t;
	code<<setw(SPACEWIDTH)<<"    "<<c<<endl;
	if(currentLoc>highestLoc)
		highestLoc=currentLoc;
}

/* function emit register-memory instruction */
void emitRM(char *opcode,int r,int d,int t,char *c){
	code<<setw(LOCWIDTH)<<currentLoc++<<":"<<setw(CODEWIDTH)<<opcode<<setw(NUMWIDTH)<<r<<","<<d<<"("<<t<<")";
	code<<setw(SPACEWIDTH)<<"    "<<c<<endl;
	if(currentLoc>highestLoc)
		highestLoc=currentLoc;
}

/* function to convert absolute address to register-relative address,take pc as target register */
void emitRM_Abs(char *opcode,int r,int d,char *c){
	int relativeLoc=d-(currentLoc+1);
	code<<setw(LOCWIDTH)<<currentLoc++<<":"<<setw(CODEWIDTH)<<opcode<<setw(NUMWIDTH)<<r<<","<<relativeLoc<<"("<<pc<<")";
	code<<setw(SPACEWIDTH)<<"    "<<c<<endl;
	if(currentLoc>highestLoc)
		highestLoc=currentLoc;
}

/* function to skip specific number of instruction location */
int emitSkip(int num){
	if(num<0)
		cout<<"emitSkip error"<<endl;
	currentLoc+=num;
	if(currentLoc>highestLoc)
		highestLoc=currentLoc;
	return currentLoc;
}
/* function to set current instruction location as backpatch instruction location */
void emitBackpatch(int loc){
	if(loc<0)
		cout<<"emitBackpatch error"<<endl;
	currentLoc=loc;
}

/* function to restore current instruction location after backpatching */
void emitRestore(){
	if(currentLoc<=highestLoc)
		currentLoc=highestLoc;
	else{
		cout<<"emitRestore error"<<endl;
		cout<<"cur:"<<currentLoc<<" "<<"high:"<<highestLoc<<endl;
	}
}

/* function to return current location */
int emitLocation(){
	return currentLoc;
}
