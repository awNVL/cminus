/* C Minus code generator - header */
#ifndef CODEGEN_H
#define CODEGEN_H
#include "globals.h"
#include "symbol.h"
#include "analyzer.h"
#include "code.h"

/* function to generate code from syntax tree */
void codeGen(TreeNode *syntaxTree,char *filename);
#endif
