/* C Minus utilities - header */
#ifndef UTILITIES_H
#define UTILITIES_H
#include <cstring>
#include <cstdlib>
#include "globals.h"
#include "scanner.h"

/* Procedure to print token in information file */
void printToken(TokenType token,char *lexeme,int lineno);

/* Function to copy a string to tree node attribute */
void copyString(TreeNode *t,char *str);

/* Function to create a new function node */
TreeNode* getFunNode(FunKind kind,TokenType type=VOID);

/* Function to create a new statement node */
TreeNode* getStmtNode(StmtKind kind);

/* Function to create a new expression node */
TreeNode* getExpNode(ExpKind kind,TokenType tok=INT);

/* Procedure to print syntax tree in indention formation */
void printTree(TreeNode *tree);

/* Procedure to destroy syntax tree after code generation */
void destroyTree(TreeNode *tree);
#endif
