/* C Minus parser - definition */
#include <cstring>
#include "globals.h"
#include "scanner.h"
#include "parser.h"

static TokenType token;		// holds current token

static TreeNode* declaration_list();
static TreeNode* declaration();
static TreeNode* var_declaration();
static TreeNode* fun_declaration();
static TreeNode* params();
static TreeNode* param_list();
static TreeNode* param();
static TreeNode* compound_stmt();
static TreeNode* local_declarations();
static TreeNode* statement_list();
static TreeNode* statement();
static TreeNode* expression_stmt();
static TreeNode* selection_stmt();
static TreeNode* iteration_stmt();
static TreeNode* return_stmt();
static TreeNode* expression();
static TreeNode* simple_expression();
static TreeNode* additive_expression();
static TreeNode* term();
static TreeNode* factor();
static TreeNode* args();
static TreeNode* arg_list();

/* Function to get a token without moving current file position */
TokenType readPrevToken(){
	int num;
	TokenType tok;
	if(!readtoken.eof()){
		int origin=readtoken.tellg();
		int distance=WIDTH*2+TOKENLEN;
		readtoken.seekg(-distance,std::ios::cur);
		readtoken>>num;
		readtoken>>lineNo;
		tok=static_cast<TokenType>(num);
		if((tok==ID)||(tok==NUM))
			readtoken>>tokenLexeme;
		readtoken.seekg(origin-readtoken.tellg(),std::ios::cur);
	}
	return tok;
}

/* Function to get a token from token file and can easily *
 * back up,to provide a interface for further process     */
TokenType currentToken(){
	int num;
	if(!readtoken.eof()){
		int origin=readtoken.tellg();
		int deter=origin+TOKENLEN+WIDTH*2;
		int here=origin;
		readtoken>>num;
		readtoken>>lineNo;
		token=static_cast<TokenType>(num);
		if((token==ID)||(token==NUM))
			readtoken>>tokenLexeme;
		readtoken.seekg(deter-readtoken.tellg(),std::ios::cur);
	}
	return token;
}

/* For very special back up case */
int getPos(){
	int origin=0;
	if(!readtoken.eof()){
		origin=readtoken.tellg();
	}
	return origin;
}

/* Procedure to move current token back up once */
void tokenBackup(){
	if(!readtoken.eof()){
		int backlen=TOKENLEN+WIDTH*2;
		int origin=readtoken.tellg();
		int backupPos=origin-backlen;
		if(backupPos<=0){
	 		readtoken.seekg(0,std::ios::beg);
	 		return;
		}
		readtoken.seekg(-backlen,std::ios::cur);
		token=readPrevToken();	// restore the token	
	}
}

/* Procedure to move current token back up to specified *
 * position,used to handle really speacial case         */
void tokenBackup(int pos){
	if(!readtoken.eof()){
		int origin=readtoken.tellg();
		if(pos>=0)
			readtoken.seekg(pos-origin,std::ios::cur);
		token=readPrevToken();	// restore the token
	}
}

static void errorMsg(){
	cout<<"in line :"<<lineNo<<"  "<<"token error:"<<token<<endl;
}

static bool match(TokenType expected){
	if(token!=expected){
		errorMsg();
		return false;
	}
	else{
		token=currentToken();
		return true;
	}
}

static TreeNode* declaration_list(){
	//info<<"In declaration_list"<<endl;
	TreeNode *t=declaration();
	TreeNode *p=t;
	if(t!=NULL){
		while((token==INT)||(token==VOID)){
			p->sibling=declaration();
			p=p->sibling;
		}
	}
	//info<<"Out declaration_list"<<endl;
	return t;
}

static TreeNode* declaration(){
	//info<<"In declaration"<<endl;
	TreeNode *t=NULL;
	if((token==INT)||(token==VOID)){
		match(token);
		match(ID);
		if((token==SIMI)||(token==LSB)){
			tokenBackup();	// now token is identifier 
			tokenBackup();	// now token is type specifier
			t=var_declaration();
		}
		else if(token==LP){
			tokenBackup();	// now token is identifier 
			tokenBackup();	// now token is type specifier
			t=fun_declaration();
		}
		else
			errorMsg();
	}
	//info<<"Out declaration"<<endl;
	return t;
}

static TreeNode* var_declaration(){
	//info<<"In var_declaration"<<endl;
	TreeNode *t=NULL;
	TokenType type;
	if((token==INT)||(token==VOID)){
		type=token;
		match(token);
		t=getExpNode(IdE,type);
		match(ID);
		if(token==LSB){
			match(token);
			t->subkind.exp=ArrE;
			t->attr.val=atoi(tokenLexeme);
			match(NUM);
			match(RSB);
		}
		match(SIMI);
	}
	//info<<"Out var_declaration"<<endl;
	return t;
}

static TreeNode* fun_declaration(){
	//info<<"In fun_declaration"<<endl;
	TreeNode *t=NULL;
	TokenType type;
	if((token==INT)||(token==VOID)){
		type=token;
		match(token);
		t=getFunNode(DeclF,type);
		match(ID);
		match(LP);
		t->child[0]=params();
		match(RP);
		t->child[1]=compound_stmt();
	}
	//info<<"Out var_declaration"<<endl;
	return t;
}

static TreeNode* params(){
	//info<<"In params"<<endl;
	TreeNode *t=NULL;
	TokenType type;
	if(token==VOID){
		type=token;
		match(token);
		t=NULL;
	}
	else if(token==RP)
		t=NULL;
	else
		t=param_list();
	//info<<"Out params"<<endl;
	return t;
}

static TreeNode* param_list(){
	//info<<"In param_list"<<endl;
	TreeNode *t=param();
	TreeNode *p=t;
	if(t!=NULL){
		while(token==COMMA){
			match(token);
			p->sibling=param();
			p=p->sibling;
		}
	}
	//info<<"Out param_list"<<endl;
	return t;
}

static TreeNode* param(){
	//info<<"In param"<<endl;
	TreeNode *t=NULL;
	TokenType type;
	if((token==INT)||(token==VOID)){
		type=token;
		match(token);
		t=getExpNode(IdE,type);
		match(ID);
		if(token==LSB){
			t->subkind.exp=ArrE;
			match(LSB);
			match(RSB);
		}
	}
	//info<<"Out param"<<endl;
	return t;
}

static TreeNode* compound_stmt(){
	//info<<"In compound_stmt"<<endl;
	match(LCB);
	TreeNode *t=getStmtNode(CompS);
	if(t!=NULL){
		t->child[0]=local_declarations();
		t->child[1]=statement_list();
	}
	match(RCB);
	//info<<"Out compound_stmt"<<endl;
	return t;
}

static TreeNode* local_declarations(){
	//info<<"In local_declarations"<<endl;
	TreeNode *t=NULL;
	if((token==INT)||(token==VOID))
		t=var_declaration();
	else{
		//info<<"Out local_declarations"<<endl;
		return t;
	}
	TreeNode *p=t;
	if(t!=NULL){
		while((token==INT)||(token==VOID)){
			p->sibling=var_declaration();
			p=p->sibling;
		}
	}
	//info<<"Out local_declarations"<<endl;
	return t;
}

static TreeNode* statement_list(){
	//info<<"In statement_list"<<endl;
	TreeNode *t=NULL;
	if((token==SIMI)||(token==ID)||(token==LP)||(token==NUM)||(token==IF)||(token==WHILE)||(token==RETURN)||(token==LCB))
		t=statement();
	else{
		//info<<"Out statement_list"<<endl;
		return t;
	}
	TreeNode *p=t;
	if(t!=NULL){
		while((token==SIMI)||(token==ID)||(token==LP)||(token==NUM)||(token==IF)||(token==WHILE)||(token==RETURN)||(token==LCB)){
			p->sibling=statement();
			p=p->sibling;
		}
	}
	//info<<"Out statement_list"<<endl;
	return t;
}

static TreeNode* statement(){
	//info<<"In statement"<<endl;
	TreeNode *t=NULL;
	switch(token){
		case SIMI:case ID:case LP:case NUM:
			t=expression_stmt();
			break;
		case LCB:
			t=compound_stmt();
			break;
		case IF:
			t=selection_stmt();
			break;
		case WHILE:
			t=iteration_stmt();
			break;
		case RETURN:
			t=return_stmt();
			break;
		default:
			errorMsg();
			break;
	}
	//info<<"Out statement"<<endl;
	return t;
}

static TreeNode* expression_stmt(){
	//info<<"In expression_stmt"<<endl;
	TreeNode *t=getStmtNode(ExpS);
	if(t!=NULL){
		if(token==SIMI){
			match(token);
			t->child[0]=NULL;;
		}
		else if((token==ID)||(token==LP)||(token==NUM)){
			t->child[0]=expression();
			match(SIMI);
		}
		else
			errorMsg();
	}
	//info<<"Out expression_stmt"<<endl;
	return t;
}

static TreeNode* selection_stmt(){
	//info<<"In selection_stmt"<<endl;
	match(IF);
	match(LP);
	TreeNode *t=getStmtNode(IfS);
	if(t!=NULL){
		t->child[0]=expression();
		match(RP);
		t->child[1]=statement();
		if(token==ELSE){
			match(token);
			t->child[2]=statement();
		}
	}
	//info<<"Out selection_stmt"<<endl;
	return t;
}

static TreeNode* iteration_stmt(){
	//info<<"In iteration_stmt"<<endl;
	match(WHILE);
	match(LP);
	TreeNode *t=getStmtNode(WhileS);
	if(t!=NULL){
		t->child[0]=expression();
		match(RP);
		t->child[1]=statement();
	}
	//info<<"Out iteration_stmt"<<endl;
	return t;
}

static TreeNode* return_stmt(){
	//info<<"In return_stmt"<<endl;
	match(RETURN);
	TreeNode *t=getStmtNode(ReturnS);
	if(t!=NULL){
		if(token==SIMI)
			t->child[0]=NULL;
		else
			t->child[0]=expression();
		match(SIMI);
	}
	//info<<"Out return_stmt"<<endl;
	return t;
}

static TreeNode* expression(){
	//info<<"In expression"<<endl;
	TreeNode *t=NULL;
	TreeNode *p=NULL;
	TreeNode *s=NULL;
	if((token==LP)||(token==NUM)){
		t=simple_expression();
		return t;
	}
	if(token==ID){
		int backFirsttime=getPos();
		t=getExpNode(AssignE);
		s=p=t;
		match(token);
		if(token==LSB){
			match(token);
			t->child[0]=expression();
			match(RSB);
		}
		if(token==ASSIGN){
			match(token);
			if((token==LP)||(token==NUM)){
				p=simple_expression();
				t->child[1]=p;
				return t;
			}
			while(token==ID){
				int origin=getPos();
				p=getExpNode(AssignE);
				match(token);
				if(token==LSB){
					match(token);
					//info<<"r"<<endl;
					p->child[0]=expression();
					//info<<"s"<<endl;
					match(RSB);
					//info<<"t"<<endl;
				}
				if(token==ASSIGN){
					match(ASSIGN);
					t->child[1]=p;
					t=p;
				}
				else{
					tokenBackup(origin);
					delete p;
					p=simple_expression();
					t->child[1]=p;
					t=s;
					return t;
				}
			}
			p=simple_expression();
			t->child[1]=p;
			t=s;		// restore
		}
		else{
			tokenBackup(backFirsttime);
			delete t;
			t=simple_expression();
		}
	}
	//info<<"Out expression"<<endl;
	return t;
}

static TreeNode* simple_expression(){
	//info<<"In simple_expression"<<endl;
	TreeNode *t=additive_expression();
	if((token==LE)||(token==LT)||(token==GT)||(token==GE)||(token==EQ)||(token==NE)){
		TokenType relop=token;
		match(token);
		TreeNode *p=getExpNode(OpE,relop);
		p->child[0]=t;
		p->child[1]=additive_expression();
		t=p;
	}
	//info<<"Out simple_expression"<<endl;
	return t;
}

static TreeNode* additive_expression(){
	//info<<"In additive_expression"<<endl;
	TreeNode *t=term();
	TreeNode *p=t;
	TokenType addop;
	while((token==PLUS)||(token==MINUS)){
		addop=token;
		match(token);
		p=getExpNode(OpE,addop);
		p->child[0]=t;
		p->child[1]=term();
		t=p;
	}
	//info<<"Out additive_expression"<<endl;
	return t;
}

static TreeNode* term(){
	//info<<"In term"<<endl;
	TreeNode *t=factor();
	TreeNode *p=t;
	TokenType mulop;
	while((token==TIMES)||(token==OVER)){
		mulop=token;
		match(token);
		p=getExpNode(OpE,mulop);
		p->child[0]=t;
		p->child[1]=factor();
		t=p;
	}
	//info<<"Out term"<<endl;
	return t;
}

static TreeNode* factor(){
	//info<<"In factor"<<endl;
	TreeNode *t=NULL;
	switch(token){
		case LP:
			match(token);
			t=expression();
			match(RP);
			break;
		case NUM:
			t=getExpNode(ConstE);
			match(token);
			break;
		case ID:
			t=getExpNode(IdE);
			match(token);
			if(token==LSB){		// case 1,ID [expression]
				match(token);
				t->subkind.exp=ArrE;
				t->child[0]=expression();
				match(RSB);
			}
			else if(token==LP){		// case 2,ID (args)
				match(token);
				t->nodekind=FunK;
				t->subkind.fun=CallF;
				t->child[0]=args();
				match(RP);
			}
			else		// case 3,ID,do nothing
				;
			break;
		default:
			errorMsg();
			break;
	}
	//info<<"Out factor"<<endl;
	return t;
}

static TreeNode* args(){
	//info<<"In args"<<endl;
	TreeNode *t=NULL;
	if((token==ID)||(token==LP)||(token==NUM))
		t=arg_list();
	//info<<"Out args"<<endl;
	return t;
}

static TreeNode* arg_list(){
	//info<<"In arg_list"<<endl;
	TreeNode *t=expression();
	TreeNode *p=t;
	if(t!=NULL){
		while(token==COMMA){
			match(token);
			p->sibling=expression();
			p=p->sibling;
		}
	}
	//info<<"Out arg_list"<<endl;
	return t;
}

TreeNode* parse(){
	TreeNode *syntaxTree=NULL;
	token=currentToken();
	syntaxTree=declaration_list();
	if(syntaxTree==NULL){
		cout<<"the first declaration maybe incorrect,check source"<<endl;
		exit(1);
	}
	info<<endl<<endl;
	return syntaxTree;
}
