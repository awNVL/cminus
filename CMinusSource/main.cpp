/* C Minus - main */
#include <cstdlib>
#include "globals.h"
#include "utilities.h"
#include "scanner.h"
#include "parser.h"
#include "analyzer.h"
#include "codegen.h"

const int FILELEN=100;

ifstream source;
ofstream info;
ofstream temp;
ofstream code;
ifstream readtoken;
TreeNode *funcTree;

int main(){
	cout<<"please input the absolute address of the file you wanna compile,and it's name:"<<endl;
	char sourcefile[FILELEN]="\0";
	char infofile[FILELEN]="\0";
	char tokenfile[FILELEN]="\0";
	char codefile[FILELEN]="\0";
	cin.getline(sourcefile,FILELEN);
	int length=strcspn(sourcefile,".");
	strncpy(infofile,sourcefile,length);
	strncpy(tokenfile,sourcefile,length);
	strncpy(codefile,sourcefile,length);
	strcat(infofile," info.txt");
	strcat(tokenfile," temp.txt");
	strcat(codefile,".cmvm");
	
	source.open(sourcefile);
	if(!source){
		cout<<"source file open failed,check if it exist: "<<sourcefile<<endl;
		exit(1);
	}
	info.open(infofile);
	temp.open(tokenfile);
	if(!temp.is_open()){
		cout<<"temp file first time open failed:"<<tokenfile<<endl;
		exit(1);
	}
	if(!info.is_open()){
		cout<<"information file open failed:"<<infofile<<endl;
		exit(1);
	}
	while(getToken()!=ENDFILE);
	temp.close();
	readtoken.open(tokenfile);
	if(!readtoken){
		cout<<"temp file second time open failed,check if it exist:"<<tokenfile<<endl;
		exit(1);
	}
	TreeNode *syntaxTree=NULL;
	syntaxTree=parse();
	printTree(syntaxTree);
	buildSymbolTable(syntaxTree);
	typeChecking(syntaxTree);
	
	code.open(codefile);
	if(!code.is_open()){
		cout<<"code open failed:"<<codefile<<endl;
		exit(1);
	}
	funcTree=syntaxTree;
	codeGen(syntaxTree,sourcefile);
	cout<<"compilation done"<<endl;
	destroyTree(syntaxTree);
	syntaxTree=NULL;
	readtoken.close();
	source.close();
	info.close();
	code.close();
	return 0;
}
