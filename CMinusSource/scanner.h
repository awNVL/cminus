/* C Minus scanner - header */
#ifndef SCANNER_H
#define SCANNER_H

const int TOKENLEN=50;				// max length of a token
extern char tokenLexeme[TOKENLEN];

/* Function to get a token each time from input *
 * and ouput to token file   */
TokenType getToken();
#endif
